import {add, subtract, multiply, divide} from "../calculations"
import {describe, test, expect} from "vitest"


describe("addition", () => {

    test("when two integers are added should return sum", () => {
        // Given
        const x = 15;
        const y = 8;

        // When
        const sum = add(x, y)

        // Then
        expect(sum).toBe(23)
    })

    test("when three integers are added, should return sum", () => {
        // Given
        const x = 5;
        const y = 7;
        const z = 10
        
        // When
        const sum = add(x, y, z);

        // Then
        expect(sum).toBe(22)

    })

    test("should throw error when too few argument", () => {
        // Given
        const x = 5;

        // When, Then
        expect(() => add(x)).toThrowError("Must contain atleast 2 arguments")

    })

    // add skall bara tillåta tal 0 - 999
    // gränsvärden att testa: -1, 0, 1 & 998, 999, 1000
    // testar de giltiga värdena 
    test.each([
        [0, 0, 0],
        [1, 1, 2],
        [998, 998, 1996],
        [999, 999, 1998]
    ])("add %i + %i should return %i", (x, y, expected) => {
        //When
        const sum = add(x, y)

        //Then
        expect(sum).toBe(expected)
    })

    // add skall bara tillåta tal 0 - 999
    // gränsvärden att testa: -1, 0, 1 & 998, 999, 1000
    // testar de ogiltiga värdena 
    test.each([
        [-1, 0],
        [0, -1],
        [1000, 0],
        [0, 1000]
    ])("add %i + %i should throw", (x, y) => {
        //When, Then
        expect(() => add(x, y)).toThrowError("Valid range is 0-999")
    })

})