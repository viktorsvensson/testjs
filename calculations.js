const add = (...args) => {
    if(args.length < 2){
        throw new Error("Must contain atleast 2 arguments")
    }
    if(args.some(val => val < 0 || val > 999)){
        throw new Error("Valid range is 0-999")
    }
    return args.reduce((prev, current) => prev + current, 0)
}


export {
    add
}

